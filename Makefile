init:
	pip install -r requirements.txt . && \
    imageio_download_bin all

editable:
	pip install -r requirements.txt -e . && \
    imageio_download_bin all

test: init
	pytest test

coverage: init
	pytest --cov=plenpy --cov-report term-missing

uninstall:
	pip uninstall plenpy
